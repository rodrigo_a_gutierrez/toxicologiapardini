# Projeto Toxicologia Pardini.

Criar branch local, digite o camando abaixo: 

### git clone
```
git clone https://rodrigo_a_gutierrez@bitbucket.org/rodrigo_a_gutierrez/toxicologiapardini.git
```

Mais informações na documentação https://git-scm.com/docs/ 

# Requisitos: 

Laravel deve estar instalado: [Laravel](https://laravel.com/docs/6.x)

## Vue-cli

Deve ser alterada o arquivo packge.json com para leitura da ferramenta npm para a instalação do vue-cli considerando que os modulos do laravel possuem a instalação global do vue-cli, consulte documentação acima para instalação de modulos globais no item "Requisitos".
O arquivo packe.json deve conter a linha "vue": "^2.1.10", lembrando que como é um arquivo json os itens devem estar separados por virgula(,), a sessão do arquivo que deve ser modificada é a "devDependencies".
Após a mesma deve ser rodado o comando abaixo:
```
npm install
```
Isso ira atualizar os pacotes dentro da solução.

## Base de dados

É preciso executar a migração de dados da solução para utilizar a mesma, que no caso de ambiente de desenvolvimento inclui dados ficticios para inicio dos trabalhos.
Um passo anterior é criar no mysql a base de dados toxicologiapardini, toda migração ira depender dessa base de dados, o comando SQL utilizado para o mesmo é:
```
CREATE DATABASE toxicologiapardini
```
Assim basta rodar no terminal os comandos:
```
php7.2 artisan migrate
php7.2 artisan db:seed
```

obs: o item php7.2 deve ser alterado para php, sendo que o mesmo inclui a versão do php que a solução esta sendo produzida e assim homologada.

# URL de utilização

Digitar no browser(navegador da internet), o endereço localhost:8080 para utilização da interface de edição de dados.

### Importante:
Certificar-se que o artisan está no ar, a linha abaixo deve constar em um terminal(bash ou cmd).
```
php7.2 artisan serve
```
obs: o item php7.2 deve ser alterado para php, sendo que o mesmo inclui a versão do php que a solução esta sendo produzida e assim homologada.

# Utilização: 

Os campos disponiveis para Nome, Sobrenome, Email, Data de Nascimento e até 6 Telefones e endereço poderam ser editados pelo endereço [Edição](http://127.0.0.1:8000/register), o endpoint esta no endereço [endpoint](http://127.0.0.1:8000/api/contact).