<?php

use Illuminate\Http\Request;
use App\Contact;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('contact', function (Request $request) { 
    $contact = Contact::all();
    return $contact;
});

Route::post('contact/save', function (Request $request) { 
    return var_dump( $request );
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
