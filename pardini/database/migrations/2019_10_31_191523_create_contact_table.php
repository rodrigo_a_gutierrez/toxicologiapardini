<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contact', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('firstName', 60);
            $table->string('lastName', 60);
            $table->date('birthdate');
            $table->string('email', 100);
            $table->string('ddd1st', 3);
            $table->string('phone1st', 9);
            $table->string('ddd2nd', 3);
            $table->string('phone2nd', 9); 
            $table->string('ddd3rd', 3);
            $table->string('phone3rd', 9);
            $table->string('ddd4th', 3);
            $table->string('phone4th', 9);
            $table->string('ddd5th', 3);
            $table->string('phone5th', 9);
            $table->string('ddd6th', 3);
            $table->string('phone6th', 9);
            $table->softDeletes();
            $table->timestamps();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contact');
    }
}
