<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        for($i=0; $i <= 10; $i++) {
            DB::table('contacts')->insert([
                'firstName' => Str::random(60),
                'lastName'  => Str::random(60),
                'email'     => Str::random(90).'@gmail.com',
                'birthdate' => '2019-01-01',
                'ddd1st'    => Str::random(3),
                'phone1st'  => Str::random(9),
                'ddd2nd'    => Str::random(3),
                'phone2nd'  => Str::random(9),
                'ddd3rd'    => Str::random(3),
                'phone3rd'  => Str::random(9),
                'ddd4th'    => Str::random(3),
                'phone4th'  => Str::random(9),
                'ddd5th'    => Str::random(3),
                'phone5th'  => Str::random(9),
                'ddd6th'    => Str::random(3),
                'phone6th'  => Str::random(9),
            ]);
        }
    }
}
