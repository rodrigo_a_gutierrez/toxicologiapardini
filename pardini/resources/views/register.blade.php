<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.js"></script>

        <title>Cadastro de Dados de Contato</title>    
    </head>
    <body style="text-align: center !important">
        <br>
        <div class="container" id="register-contanner">
            <div class="bg-info clearfix">
                Cadastro de Dados de Contato
            </div>
            <form>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputFirstName"> Nome : </label>
                            <input type="text" class="form-control" id="inputFirstName" aria-describedby="nameFirstHelp" placeholder="Digite o nome">
                        </div> 
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputLastName"> Sobrenome : </label>
                            <input type="text" class="form-control" id="inputLastName" aria-describedby="nameLastHelp" placeholder="Digite o sobrenome">
                        </div>
                    </div>
                </div>    
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputEmail1">  Email : </label>
                            <input type="mail" class="form-control" id="inputMail" aria-describedby="mailHelp" placeholder="Digite o e-mail">
                        </div>
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputBirthDate"> Data de Nascimento : </label>
                            <input type="text" class="form-control" id="inputBirthDate" aria-describedby="birthDateHelp" placeholder="Digite a data de nascimento">
                        </div> 
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputDDD1st"> DDD : </label>
                            <input type="text" class="form-control" id="inputDDD1st" aria-describedby="DDD1stHelp" placeholder="Digite o DDD">
                        </div>
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone1st"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone1st" aria-describedby="phone1stHelp" placeholder="Digite o telefone">
                        </div>
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                                <label for="inputDDD2nd"> DDD : </label>
                                <input type="text" class="form-control" id="inputDDD2nd" aria-describedby="DDD2ndHelp" placeholder="Digite o DDD">
                        </div>
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone2nd"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone2nd" aria-describedby="Phone2ndHelp" placeholder="Digite o telefone">
                        </div>
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputDDD3st"> DDD : </label>
                            <input type="text" class="form-control" id="inputDDD3st" aria-describedby="DDD3stHelp" placeholder="Digite o DDD">
                        </div> 
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone3st"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone3st" aria-describedby="Phone3stHelp" placeholder="Digite o telefone">
                        </div> 
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputDDD4st"> DDD : </label>
                            <input type="text" class="form-control" id="inputDDD4st" aria-describedby="DDD4stHelp" placeholder="Digite o DDD">
                        </div>
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone4st"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone4st" aria-describedby="Phone4stHelp" placeholder="Digite o telefone">
                        </div> 
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputDDD5st"> DDD : </label>
                            <input type="text" class="form-control" id="inputDDD5st" aria-describedby="DDD5stHelp" placeholder="Digite o DDD">
                        </div> 
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone5st"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone5st" aria-describedby="Phone5stHelp" placeholder="Digite o telefone">
                        </div> 
                    </div>
                </div>
                <div class="row mx-lg-n5">
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputDDD6st"> DDD : </label>
                            <input type="text" class="form-control" id="inputDDD6st" aria-describedby="DDD6stHelp" placeholder="Digite o DDD">
                        </div> 
                    </div>
                    <div class="col-sm py-3 px-lg-5">
                        <div class="form-group">
                            <label for="inputPhone6st"> Telefone : </label>
                            <input type="text" class="form-control" id="inputPhone6st" aria-describedby="Phone6stHelp" placeholder="Digite o telefone">
                        </div> 
                    </div>
                </div>
            </form>
            <div class="bg-info clearfix">
                <button type="button" class="btn btn-secondary float-right" @click="saveRegister()">Salvar Formulário</button> 
                <button type="button" class="btn btn-secondary float-center" @click="showRegister()">Exibir dados cadastrados console</button>
            </div>
        </div>
    </body>
    <script>
            let apiURI = '/api';
            let vmRegister = new Vue({
                el: '#register-contanner',
                methods: {
                        saveRegister: function () {
                            axios.post(apiURI + '/contact/save', {
                                firstName: 'Fred',
                                lastName: 'Flintstone'
                            })
                            .then(function (response) {
                                console.log(response);
                            });
                        },
                        showRegister: function () {
                            axios.get(apiURI + '/contact').then(function(response){ console.log(JSON.stringify(response));});
                            alert('teste');
                        },
                    }
            });    
    </script>
</html>